package ru.csu.task4springcontext.service;

public interface MessageRenderer {
    void render(String message);
}
