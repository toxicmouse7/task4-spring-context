package ru.csu.task4springcontext.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.csu.task4springcontext.service.HelloService;
import ru.csu.task4springcontext.service.MessageRenderer;

@Component
public class HelloServiceImpl implements HelloService {
    private final MessageRenderer renderer;


    public HelloServiceImpl(MessageRenderer renderer) {
        this.renderer = renderer;
    }


    @Override
    public void sayHello() {
        renderer.render("I am say Hello World");
    }
}
